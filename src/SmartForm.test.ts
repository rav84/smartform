import Vue from "vue";
import Vuetify from "vuetify";
import { Entity, Input } from './decorators';
import { mount } from "@vue/test-utils";
import log from "loglevel";
import SmartForm from "./SmartForm.vue";
import _ from "lodash";

// Setup
(global as any).confirm = () => true;

jest.mock("loglevel", () => {
    return {
        error: jest.fn(),
        warn: jest.fn(),
    }
})

Vue.use(Vuetify);
const Constructor = Vue.extend(SmartForm);

@Entity({})
class Child {
    @Input({ type: "input" })
    public field?: string | null;
    constructor(obj?: Child) { this.field = obj && obj.field ? obj.field : null; }
}

@Entity({ isCollection: true, type: Child })
class Children {
    [index: string]: Child;
}

@Entity({})
class E {
    @Input({ type: "input", filter: _.startCase, props: {} })
    public a?: string | null;
    @Input({ type: Child, isComposite: true })
    public b?: Child | null;
    @Input({ type: Children, isComposite: true })
    public c?: Children | null;
    constructor(obj?: E) {
        this.a = obj && obj.a ? obj.a : null;
        this.b = obj && obj.b ? obj.b : null;
        this.c = obj && obj.c ? obj.c : null
    }
}


describe("SmartForm component", () => {

    test("Can handle invalid entity", () => {
        const vm: any = new Constructor({ propsData: { entity: {} } }).$mount();
        expect(log.error).toHaveBeenCalled();
        expect(vm.isEntityValid).toEqual(false);
    });

    test("Renders valid entity", () => {
        const vm: any = new Constructor({ propsData: { entity: new E() } }).$mount();
        expect(vm.isEntityValid).toEqual(true);
    });

    test("Adding values from collections", () => {
        const wrapper = mount(SmartForm, { propsData: { entity: new E() } });
        const addChild = wrapper.find(".v-icon.green--text");
        addChild.trigger("click");

        // Testing if child has been added to c
        const entity = (wrapper.vm as any).entity;
        expect(Object.keys(entity.c).length).toEqual(1);
    });

    test("Removing values from collections", () => {
        const children = { a: new Child() };
        const wrapper = mount(SmartForm, { propsData: { entity: new E({ c: children }) } });
        const forms = wrapper.findAll(SmartForm);
        // 3rd form is collection (children)
        (forms.at(2) as any).vm.removeCollectionItem("a")

        // Testing if child has been removed from c
        const entity = (wrapper.vm as any).entity;
        console.log(entity);
        expect(Object.keys(entity.c).length).toEqual(0);
    });

    test("Filters working on inputs", () => {
        const wrapper = mount(SmartForm, { propsData: { entity: new E({ a: "abc" }) } });
        const input = wrapper.find("input"); // first input has a filter
        input.trigger("change");

        let entity = (wrapper.vm as any).entity;
        expect(entity.a).toEqual("Abc");
    });

});
