import { Entity, Input } from "./decorators";

describe("Entity & Input", () => {

    test("Successfully extends class to return Entity and FormProperty attributes", () => {

        @Entity({ name: "name" })
        class T { };
        const t = new T();

        // Testing if Entity attributes exists
        expect(t).toHaveProperty("_SmartFormEntityAttributes");

        // Testing if name property was successfully stored as Entity options.
        const entityAttributes = (t as any)._SmartFormEntityAttributes;
        expect(entityAttributes.name).toEqual("name");

        // Checking if Property attributes also exists
        expect(t).toHaveProperty("_SmartFormPropertyAttributes");
        expect((t as any)._SmartFormPropertyAttributes).not.toBeNull();
    });

    test("Sucessfully register class member attributes using @Input", () => {
        @Entity({ name: "name" })
        class T {
            @Input({ type: "VTextField", filter: (a: any) => a, props: {} })
            public prop?: string = "value";
        };
        const t = new T();

        // Testing the property exists on PropertyAttributes
        expect((t as any)._SmartFormPropertyAttributes).toHaveProperty("prop");

        // Testing type was successfully stored for the property.
        expect((t as any)._SmartFormPropertyAttributes.prop).toHaveProperty("type");

        // Testing filter was successfully stored for the property.
        expect((t as any)._SmartFormPropertyAttributes.prop).toHaveProperty("filter");

    });
});

// Input is already tested in second test of Entity
