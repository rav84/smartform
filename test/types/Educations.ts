import { Entity, Input } from "../../src/decorators";

@Entity({})
export class Education {
    @Input({ type: "VTextField", props: { label: "Education" } })
    public education: string | null;
    constructor(obj?: Education) {
        this.education = obj && obj.education ? obj.education : null;
    }
    public toString() {
        return "Education: " + this.education;
    }
}

@Entity({
    name: "Education Collection",
    desc: "Please enter below all of your educations",
    type: Education, isCollection: true
})
export class EducationCollection {
    [index: string]: Education;
}