import Vue from 'vue';
import App from './App.vue';
import Vuetify from 'vuetify';
import "vuetify/dist/vuetify.css";
import _ from "lodash";

Vue.use(Vuetify);

Vue.filter("startCase", {
    read(val) { return val; },
    write(val, oldVal) { return _.startCase(val); }
} as any);

Vue.config.productionTip = false;

const v = new Vue({
    el: "#app",
    render: h => h(App)
});

(window as any).vue = v;
